/// sを一旦すべてpopし、qに移し、sにpushし直せばよい。
use std::collections::VecDeque;
use std::iter::FromIterator;

fn main() {
    let mut s = Vec::from_iter(0..10);
    let mut q = VecDeque::new();
    println!("Before: {:?}", s);
    while !s.is_empty() {
        q.push_back(s.pop().unwrap());
    }
    while !q.is_empty() {
        s.push(q.pop_front().unwrap());
    }
    println!("After:  {:?}", s);
}
