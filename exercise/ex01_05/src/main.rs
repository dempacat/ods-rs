#![allow(dead_code)]

use std::collections::HashSet;
use std::hash::Hash;

type USet<T> = HashSet<T>;

// 問題の指定で、USet(HashSet)を使うことになっているのでそうするが、
// 所有権を考えなくていい場合は、HashMap<T, u32>で、個数を値で管理するのがいいと思う
#[derive(Debug)]
struct Bag<T: Eq + Hash + Clone>(USet<(T, u32)>);
impl<T: Eq + Hash + Clone> Bag<T> {
    fn new() -> Bag<T> {
        Bag(USet::new())
    }
    fn _find_n(&self, x: &T) -> u32 {
        let mut key = (x.clone(), 0);
        // 効率を考えるなら、2分法的アプローチを考えるべき
        for i in 0.. {
            key.1 = i;
            if !self.0.contains(&key) {
                return i;
            }
        }
        unreachable!();
    }
    fn add(&mut self, x: T) -> bool {
        let n = self._find_n(&x);
        self.0.insert((x, n));
        true
    }
    fn remove(&mut self, x: &T) -> Option<T> {
        let n = self._find_n(x);
        if n == 0 {
            return None;
        }
        self.0.take(&(x.clone(), n - 1)).map(|(k, _)| k)
    }
    fn find(&self, x: &T) -> Option<&T> {
        self.0.get(&(x.clone(), 0)).map(|(k, _)| k)
    }
    fn find_all(&self, x: &T) -> Vec<&T> {
        let mut v = vec![];
        let mut key = (x.clone(), 0);
        for i in 0.. {
            key.1 = i;
            match self.0.get(&key).map(|(k, _)| k) {
                Some(k) => {
                    v.push(k);
                }
                None => {
                    return v;
                }
            }
        }
        unreachable!();
    }
}

fn main() {
    println!("Run `cargo test`.");
}

#[test]
fn it_works() {
    let mut bag = Bag::new();

    assert_eq!(bag.find(&"foo"), None);
    assert_eq!(bag.find_all(&"foo"), Vec::<&&str>::new());
    bag.add("foo");
    assert_eq!(bag.find(&"foo"), Some(&"foo"));
    bag.add("bar");
    assert_eq!(bag.find_all(&"foo"), vec![&"foo"]);
    bag.add("foo");
    bag.add("bar");
    bag.add("bar");
    bag.add("bar");
    assert_eq!(bag.find(&"foo"), Some(&"foo"));
    assert_eq!(bag.find_all(&"foo"), vec![&"foo", &"foo"]);
    bag.remove(&"bar");
    assert_eq!(bag.remove(&"foo"), Some("foo"));
    assert_eq!(bag.find_all(&"foo"), vec![&"foo"]);
    assert_eq!(bag.remove(&"foo"), Some("foo"));
    assert_eq!(bag.find_all(&"foo"), Vec::<&&str>::new());
    assert_eq!(bag.remove(&"foo"), None);
}
