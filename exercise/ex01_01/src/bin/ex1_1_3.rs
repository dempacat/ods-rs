use std::collections::VecDeque;
use std::env::args;
use std::fs::File;
use std::io::{Error, BufRead, BufReader};

const FILENAME: &str = "1_1_3.txt";

fn main() -> Result<(), Error> {
    let f = File::open(args().nth(1).as_deref().unwrap_or(FILENAME))?;
    let f = BufReader::new(f);
    let mut lines = f.lines();
    let mut text = VecDeque::new();

    for s in lines.by_ref().take(42) {
        text.push_back(s?);
    }
    for s in lines {
        let s = s?;
        if s.is_empty() {
            println!("{}", text.front().unwrap());
        }
        text.pop_front();
        text.push_back(s);
    }
    Ok(())
}
