use std::env::args;
use std::fs::File;
use std::io::{Error, BufRead, BufReader};

const FILENAME: &str = "123.txt";

fn main() -> Result<(), Error> {
    let f = File::open(args().nth(1).as_deref().unwrap_or(FILENAME))?;
    let f = BufReader::new(f);
    let mut text = vec![];

    for (i, s) in f.lines().enumerate() {
        if i % 2 == 1 {
            text.push(s?);
        } else {
            println!("{}", s?);
        }
    }
    for s in text.into_iter() {
        println!("{}", s);
    }
    Ok(())
}
