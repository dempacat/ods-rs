use std::cmp::Ordering;
use std::collections::BTreeSet;
use std::env::args;
use std::fs::File;
use std::io::{Error, BufRead, BufReader};
use std::ops::Deref;

const FILENAME: &str = "1_1_6.txt";

#[derive(Debug, PartialEq, Eq)]
struct S (String);

impl Deref for S {
    type Target = String;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl Ord for S {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.len(), self.deref()).cmp(&(other.len(), other.deref()))
    }
}
impl PartialOrd for S {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn main() -> Result<(), Error> {
    let f = File::open(args().nth(1).as_deref().unwrap_or(FILENAME))?;
    let f = BufReader::new(f);
    let mut text = BTreeSet::new();

    for s in f.lines() {
        let s = S(s?);
        if !text.contains(&s) {
            text.insert(s);
        }
    }
    for s in text.into_iter() {
        println!("{}", *s);
    }
    Ok(())
}
