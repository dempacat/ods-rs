use std::collections::HashSet;
use std::env::args;
use std::fs::File;
use std::io::{Error, BufRead, BufReader};

const FILENAME: &str = "1_1_5.txt";

fn main() -> Result<(), Error> {
    let f = File::open(args().nth(1).as_deref().unwrap_or(FILENAME))?;
    let f = BufReader::new(f);
    let mut text = HashSet::new();

    for s in f.lines() {
        let s = s?;
        if text.contains(&s) {
            println!("{}", s);
        } else {
            text.insert(s);
        }
    }
    Ok(())
}
