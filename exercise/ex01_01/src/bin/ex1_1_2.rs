use std::env::args;
use std::fs::File;
use std::io::{Error, BufRead, BufReader};

const FILENAME: &str = "million.txt";

fn main() -> Result<(), Error> {
    let f = File::open(args().nth(1).as_deref().unwrap_or(FILENAME))?;
    let f = BufReader::new(f);
    let mut text = vec![];

    for s in f.lines() {
        text.push(s?);
        if text.len() == 50 {
            for line in text.iter().rev() {
                println!("{}", line);
            }
            text.clear();
        }
    }
    for line in text.iter().rev() {
        println!("{}", line);
    }
    Ok(())
}
