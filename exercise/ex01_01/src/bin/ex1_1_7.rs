use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::env::args;
use std::fs::File;
use std::io::{Error, BufRead, BufReader};
use std::ops::Deref;

const FILENAME: &str = "1_1_6.txt";

#[derive(Debug, PartialEq, Eq)]
struct S (String);

impl Deref for S {
    type Target = String;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl Ord for S {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.len(), self.deref()).cmp(&(other.len(), other.deref()))
    }
}
impl PartialOrd for S {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn main() -> Result<(), Error> {
    let f = File::open(args().nth(1).as_deref().unwrap_or(FILENAME))?;
    let f = BufReader::new(f);
    let mut text = BTreeMap::new();

    for s in f.lines() {
        let s = S(s?);
        text.entry(s).and_modify(|n| *n += 1).or_insert(1);
    }
    for (s, n) in text.into_iter() {
        for _ in 0..n {
            println!("{}", *s);
        }
    }
    Ok(())
}
