use std::env::args;
use std::fs::File;
use std::io::{Error, BufRead, BufReader};

const FILENAME: &str = "123.txt";

struct XorShift64(u64);

// https://ja.wikipedia.org/wiki/Xorshift
#[allow(dead_code)]
impl XorShift64 {
    fn new() -> XorShift64 {
        XorShift64(88172645463325252)
    }

    fn with_seed(seed: u64) -> XorShift64 {
        XorShift64(seed)
    }

    fn rand(&mut self) -> u64 {
        let mut x = self.0;
        x = x ^ (x << 13);
        x = x ^ (x >> 7);
        x = x ^ (x << 17);
        self.0 = x;
        x
    }
}

impl Iterator for XorShift64 {
    type Item = u64;
    fn next(&mut self) -> Option<Self::Item> {
        Some(self.rand())
    }
}

fn shuffle<T>(slice: &mut [T], rng: &mut XorShift64) {
    // Fisher-Yatesのアルゴリズムの改良版
    // https://ja.wikipedia.org/wiki/%E3%83%95%E3%82%A3%E3%83%83%E3%82%B7%E3%83%A3%E3%83%BC%E2%80%93%E3%82%A4%E3%82%A7%E3%83%BC%E3%83%84%E3%81%AE%E3%82%B7%E3%83%A3%E3%83%83%E3%83%95%E3%83%AB#%E6%94%B9%E8%89%AF%E3%81%95%E3%82%8C%E3%81%9F%E3%82%A2%E3%83%AB%E3%82%B4%E3%83%AA%E3%82%BA%E3%83%A0
    for i in (1..slice.len()).rev() {
        let j = rng.rand() as usize % (i + 1);
        slice.swap(i, j);
    }
}

fn main() -> Result<(), Error> {
    let f = File::open(args().nth(1).as_deref().unwrap_or(FILENAME))?;
    let f = BufReader::new(f);
    let mut text = vec![];

    for s in f.lines() {
        text.push(s?);
    }
    shuffle(&mut text, &mut XorShift64::new());
    for s in text.into_iter() {
        println!("{}", s);
    }
    Ok(())
}
