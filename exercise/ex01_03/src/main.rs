/// 括弧が「マッチした文字列」をO(n), nは文字列長, で判定するアルゴリズムを示す。
///
/// 文字列を先頭から1文字ずつ読み、以下の処理をする。
/// 開き括弧が現れたときに、スタックに括弧の種類をpushする。
/// 閉じ括弧が現れたときに、
/// - スタックの末尾が、現れた閉じ括弧と同じ種類のものであれば何もしない
/// - スタックが空 or 末尾が現れた閉じ括弧と異なる種類のものであれば「マッチしていない」
///   と判定し、処理を終了する
/// 開き括弧、閉じ括弧のいずれでもない場合は、何もしない
///
/// 文字列の末尾まで上記処理を行った後、スタックが空であれば「マッチした文字列」、
/// 空でなければ「マッチしていない文字列」と判定し、処理を終了する

use std::env::args;

#[derive(Debug, PartialEq, Eq)]
enum Bracket {
    Paren,
    Brace,
    SquareBracket
}

fn str_match(s: &str) -> bool {
    let mut stack = vec![];
    for c in s.chars() {
        match c {
            '(' => stack.push(Bracket::Paren),
            '{' => stack.push(Bracket::Brace),
            '[' => stack.push(Bracket::SquareBracket),
            ')' => {
                if !stack.pop().map(|b| b == Bracket::Paren).unwrap_or(false) {
                    return false;
                }
            }
            '}' => {
                if !stack.pop().map(|b| b == Bracket::Brace).unwrap_or(false) {
                    return false;
                }
            }
            ']' => {
                if !stack.pop().map(|b| b == Bracket::SquareBracket).unwrap_or(false) {
                    return false;
                }
            }
            _ => {}
        }
    }
    stack.is_empty()
}

fn main() {
    println!("{}", str_match(&args().nth(1).unwrap()));
}
